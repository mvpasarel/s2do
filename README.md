Symfony To Do App
========================

Install composer
Install symfony
Install mongodb
Install mongodb driver for PHP
Add and configure DoctrineMongoDBBundle
Add and configure FOSRestBundle 

Requirements:
php5-curl

Enjoy!

[1]:  http://getcomposer.org/
[2]:  http://symfony.com/doc/2.3/book/installation.html
[3]:  http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/
[4]:  http://docs.mongodb.org/ecosystem/drivers/php/
[5]:  http://symfony.com/doc/current/bundles/DoctrineMongoDBBundle/index.html
[6]:  https://github.com/FriendsOfSymfony/FOSRestBundle
[7]:  http://npmasters.com/2012/11/25/Symfony2-Rest-FOSRestBundle.html
